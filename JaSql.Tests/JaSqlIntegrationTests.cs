namespace JaSql.Tests
{
    using System.Data.SQLite;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class JaSqlIntegrationTests
    {
        private JaSql jaSql;

        [TestInitialize]
        public void Init()
        {
            jaSql = JaSql.FromFile(@"sql\test_schema.sql");
        }

        [TestMethod]
        public void TestDbLifetime()
        {
            using (var cn = new SQLiteConnection("Data Source=:memory:"))
            using (var jaRunner = jaSql.CreateRunner(cn))
            {
                jaRunner.Execute("create-users-table");

                using (var tx = cn.BeginTransaction())
                {
                    jaRunner.Execute("create-user", new {name = "Rick van Lieshout", email = "rick.van.lieshout at gmail.com"});
                    var user = jaRunner.Query<User>("find-one-user-by-email", new {Email = "rick.van.lieshout at gmail.com"}).FirstOrDefault();

                    Assert.IsNotNull(user);
                    Assert.AreEqual(1, user.Id);
                    Assert.AreEqual("Rick van Lieshout", user.Name);
                    Assert.AreEqual("rick.van.lieshout at gmail.com", user.Email);

                    Assert.AreEqual(1, jaRunner.ExecuteScalar<int>("count-users"));

                    tx.Rollback();
                }

                var cnt = jaRunner.ExecuteScalar<int>("count-users");
                Assert.AreEqual(0, cnt);

                jaSql.Execute(cn, "drop-users-table");
            }
        }

        public class User
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
        }
    }
}