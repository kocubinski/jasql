# JaSql #

A .NET (4.0) library for using SQL.

It is not an ORM, it is not a query builder. JaSql is a library that helps you keep SQL queries in one place and use them with ease.

JaSql is heavily inspired by [yesql](https://github.com/krisajenkins/yesql) and [Dotsql](https://github.com/gchaincl/dotsql).

## usage ##
Add some queries to a file. Each query should start with a line with a ```-- name:```  tag like so (file: _queries.sql_):

```sql
-- name: create-users-table
CREATE TABLE users (
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  name VARCHAR(255),
  email VARCHAR(255)
  );

-- name: create-user
INSERT INTO users (name, email) VALUES(@name, @email)
  
-- name: find-one-user-by-email
SELECT id,name,email FROM users WHERE email = @email LIMIT 1

-- name: count-users
SELECT COUNT(1) FROM users

-- name: drop-users-table
DROP TABLE users
```

(tip: if you use Visual Studio, add this file to your project and set ```Build Action: Content``` and ```Copy to Output Directory: Copy if newer```)

You now can create a new JaSql query repository using:

```csharp
var jaSql = JaSql.FromFile("queries.sql");
```

All you need to do now is create a [```DbConnection```](http://msdn.microsoft.com/en-us/library/system.data.common.dbconnection(v=vs.100).aspx) to your database, create a runner and fire away some queries (example uses SQLite):

```csharp
public class User
{
  public int Id { get; set; }
  public string Name { get; set; }
  public string Email { get; set; }
}

using (var cn = new SQLiteConnection("<your connection string here>"))
using (var jaRunner = jaSql.CreateRunner(cn))
{
  jaRunner.Execute("create-users-table");
  
  jaRunner.Execute("create-user", new {name = "John Doe", email = "j.doe@verymuchnotalive.com"});
  var user = jaRunner.Query<User>("find-one-user-by-email", new {Email = "j.doe@verymuchnotalive.com"}).First();
}
```

### see also ###

* [SQLite connection strings](http://www.connectionstrings.com/sqlite/)