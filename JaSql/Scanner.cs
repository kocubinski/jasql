﻿namespace JaSql
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;

    internal class Scanner
    {
        private readonly StreamReader inputStream;
        private readonly Dictionary<string, string> queries = new Dictionary<string, string>();
        private string line;
        private string current;

        public Scanner(StreamReader inputStream)
        {
            this.inputStream = inputStream;
        }

        public Dictionary<string, string> Run()
        {
            while (true)
            {
                line = inputStream.ReadLine();
                if (line == null) break;

                var tag = GetTag(line);
                if (tag != null) { current = tag; continue; }
                if (current != null) AppendQueryLine();
            }

            return queries;
        }

        internal static string GetTag(string s)
        {
            var m = new Regex("^\\s*--\\sname:\\s*(\\S+)").Match(s);
            return m.Success ? m.Groups[1].ToString() : null;
        }

        private void AppendQueryLine()
        {
            if (!queries.ContainsKey(current))
                queries.Add(current, line);
            else
                queries[current] += '\n' + line;
        }
    }
}