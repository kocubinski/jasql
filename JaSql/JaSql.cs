﻿namespace JaSql
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Text;
    using Dapper;

    public class JaSql
    {
        private Dictionary<string, string> queries = new Dictionary<string, string>();

        private JaSql()
        {
        }

        public static JaSql FromStreamReader(StreamReader reader)
        {
            return new JaSql().Load(reader);
        }

        public static JaSql FromFile(string path)
        {
            using (var fs = File.OpenText(path))
                return new JaSql().Load(fs);
        }

        public static JaSql FromString(string sqlFile)
        {
            return new JaSql().Load(sqlFile);
        }

        public string Raw(string name)
        {
            return LookupQuery(name);
        }

        public IEnumerable<dynamic> Query(IDbConnection cnn, string name, object param = null, IDbTransaction transaction = null, bool buffered = false, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (cnn.EnsureOpen())
                return cnn.Query(LookupQuery(name), param, transaction, buffered, commandTimeout, commandType);
        }

        public IEnumerable<T> Query<T>(IDbConnection cnn, string name, object param = null, IDbTransaction transaction = null, bool buffered = false, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (cnn.EnsureOpen())
                return cnn.Query<T>(LookupQuery(name), param, transaction, buffered, commandTimeout, commandType);
        }

        public IEnumerable<TReturn> Query<T1, T2, TReturn>(IDbConnection cnn, string name, Func<T1, T2, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        {
            using (cnn.EnsureOpen())
                return cnn.Query(LookupQuery(name), map, param, transaction, buffered, splitOn, commandTimeout, commandType);
        }

        public IEnumerable<TReturn> Query<T1, T2, T3, TReturn>(IDbConnection cnn, string name, Func<T1, T2, T3, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        {
            using (cnn.EnsureOpen())
                return cnn.Query(LookupQuery(name), map, param, transaction, buffered, splitOn, commandTimeout, commandType);
        }

        public IEnumerable<TReturn> Query<T1, T2, T3, T4, TReturn>(IDbConnection cnn, string name, Func<T1, T2, T3, T4, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        {
            using (cnn.EnsureOpen())
                return cnn.Query(LookupQuery(name), map, param, transaction, buffered, splitOn, commandTimeout, commandType);
        }

        public IEnumerable<TReturn> Query<T1, T2, T3, T4, T5, TReturn>(IDbConnection cnn, string name, Func<T1, T2, T3, T4, T5, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        {
            using (cnn.EnsureOpen())
                return cnn.Query(LookupQuery(name), map, param, transaction, buffered, splitOn, commandTimeout, commandType);
        }

        public IEnumerable<TReturn> Query<T1, T2, T3, T4, T5, T6, TReturn>(IDbConnection cnn, string name, Func<T1, T2, T3, T4, T5, T6, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        {
            using (cnn.EnsureOpen())
                return cnn.Query(LookupQuery(name), map, param, transaction, buffered, splitOn, commandTimeout, commandType);
        }

        public IEnumerable<TReturn> Query<T1, T2, T3, T4, T5, T6, T7, TReturn>(IDbConnection cnn, string name, Func<T1, T2, T3, T4, T5, T6, T7, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        {
            using (cnn.EnsureOpen())
                return cnn.Query(LookupQuery(name), map, param, transaction, buffered, splitOn, commandTimeout, commandType);
        }

        public int Execute(IDbConnection cnn, string name, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (cnn.EnsureOpen())
                return cnn.Execute(LookupQuery(name), param, transaction, commandTimeout, commandType);
        }

        public object ExecuteScalar(IDbConnection cnn, string name, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (cnn.EnsureOpen())
                return cnn.ExecuteScalar(LookupQuery(name), param, transaction, commandTimeout, commandType);
        }

        public T ExecuteScalar<T>(IDbConnection cnn, string name, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (cnn.EnsureOpen())
                return cnn.ExecuteScalar<T>(LookupQuery(name), param, transaction, commandTimeout, commandType);
        }

        public SqlMapper.GridReader QueryMultiple(IDbConnection cnn, string name, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (cnn.EnsureOpen())
                return cnn.QueryMultiple(LookupQuery(name), param, transaction, commandTimeout, commandType);
        }

        private JaSql Load(string sql)
        {
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(sql ?? "")))
            using (var sr = new StreamReader(ms, Encoding.UTF8))
                return Load(sr);
        }

        private JaSql Load(StreamReader sr)
        {
            queries = new Scanner(sr).Run();
            return this;
        }

        private string LookupQuery(string name)
        {
            if (!queries.ContainsKey(name))
                throw new KeyNotFoundException(string.Format("JaSql: '{0}' could not be found.", name));
            return queries[name];
        }

        public IRunner CreateRunner(IDbConnection connection)
        {
            if (connection == null) throw new ArgumentNullException("connection");
            return new Runner(this, connection);
        }

        public interface IRunner : IDisposable
        {
            IEnumerable<object> Query(string name, object param = null, IDbTransaction transaction = null, bool buffered = false, int? commandTimeout = null, CommandType? commandType = null);
            IEnumerable<T> Query<T>(string name, object param = null, IDbTransaction transaction = null, bool buffered = false, int? commandTimeout = null, CommandType? commandType = null);
            IEnumerable<TReturn> Query<T1, T2, TReturn>(string name, Func<T1, T2, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
            IEnumerable<TReturn> Query<T1, T2, T3, TReturn>(string name, Func<T1, T2, T3, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
            IEnumerable<TReturn> Query<T1, T2, T3, T4, TReturn>(string name, Func<T1, T2, T3, T4, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
            IEnumerable<TReturn> Query<T1, T2, T3, T4, T5, TReturn>(string name, Func<T1, T2, T3, T4, T5, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
            IEnumerable<TReturn> Query<T1, T2, T3, T4, T5, T6, TReturn>(string name, Func<T1, T2, T3, T4, T5, T6, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
            IEnumerable<TReturn> Query<T1, T2, T3, T4, T5, T6, T7, TReturn>(string name, Func<T1, T2, T3, T4, T5, T6, T7, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
            int Execute(string name, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
            object ExecuteScalar(string name, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
            T ExecuteScalar<T>(string name, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
            SqlMapper.GridReader QueryMultiple(string name, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        }

        private class Runner : IRunner
        {
            private readonly JaSql jaSql;
            private readonly IDbConnection cnn;
            private IDisposable openCnn;

            internal Runner(JaSql jaSql, IDbConnection connection)
            {
                this.jaSql = jaSql;
                cnn = connection;
                openCnn = cnn.EnsureOpen();
            }

            public void Dispose()
            {
                if (openCnn != null)
                {
                    openCnn.Dispose();
                    openCnn = null;
                }
            }

            public IEnumerable<dynamic> Query(string name, object param = null, IDbTransaction transaction = null, bool buffered = false, int? commandTimeout = null, CommandType? commandType = null)
            {
                return cnn.Query(LookupQuery(name), param, transaction, buffered, commandTimeout, commandType);
            }

            public IEnumerable<T> Query<T>(string name, object param = null, IDbTransaction transaction = null, bool buffered = false, int? commandTimeout = null, CommandType? commandType = null)
            {
                return cnn.Query<T>(LookupQuery(name), param, transaction, buffered, commandTimeout, commandType);
            }

            public IEnumerable<TReturn> Query<T1, T2, TReturn>(string name, Func<T1, T2, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
            {
                return cnn.Query(LookupQuery(name), map, param, transaction, buffered, splitOn, commandTimeout, commandType);
            }

            public IEnumerable<TReturn> Query<T1, T2, T3, TReturn>(string name, Func<T1, T2, T3, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
            {
                return cnn.Query(LookupQuery(name), map, param, transaction, buffered, splitOn, commandTimeout, commandType);
            }

            public IEnumerable<TReturn> Query<T1, T2, T3, T4, TReturn>(string name, Func<T1, T2, T3, T4, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
            {
                return cnn.Query(LookupQuery(name), map, param, transaction, buffered, splitOn, commandTimeout, commandType);
            }

            public IEnumerable<TReturn> Query<T1, T2, T3, T4, T5, TReturn>(string name, Func<T1, T2, T3, T4, T5, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
            {
                return cnn.Query(LookupQuery(name), map, param, transaction, buffered, splitOn, commandTimeout, commandType);
            }

            public IEnumerable<TReturn> Query<T1, T2, T3, T4, T5, T6, TReturn>(string name, Func<T1, T2, T3, T4, T5, T6, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
            {
                return cnn.Query(LookupQuery(name), map, param, transaction, buffered, splitOn, commandTimeout, commandType);
            }

            public IEnumerable<TReturn> Query<T1, T2, T3, T4, T5, T6, T7, TReturn>(string name, Func<T1, T2, T3, T4, T5, T6, T7, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = false, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
            {
                return cnn.Query(LookupQuery(name), map, param, transaction, buffered, splitOn, commandTimeout, commandType);
            }

            public int Execute(string name, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
            {
                return cnn.Execute(LookupQuery(name), param, transaction, commandTimeout, commandType);
            }

            public object ExecuteScalar(string name, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
            {
                return cnn.ExecuteScalar(LookupQuery(name), param, transaction, commandTimeout, commandType);
            }

            public T ExecuteScalar<T>(string name, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
            {
                return cnn.ExecuteScalar<T>(LookupQuery(name), param, transaction, commandTimeout, commandType);
            }

            public SqlMapper.GridReader QueryMultiple(string name, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
            {
                return cnn.QueryMultiple(LookupQuery(name), param, transaction, commandTimeout, commandType);
            }

            private string LookupQuery(string name)
            {
                return jaSql.Raw(name);
            }
        }
    }
}