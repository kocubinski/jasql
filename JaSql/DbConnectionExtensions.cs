﻿namespace JaSql
{
    using System;
    using System.Data;

    internal static class DbConnectionExtensions
    {
        /// <summary>
        /// Opens the <paramref name="connection"/> (if it's not open) and returns an <see cref="IDisposable"/> object
        /// you can use to close it (if it wasn't open).
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>An <see cref="IDisposable"/> object to close the connection.</returns>
        /// <remarks>
        /// Use this method with the <c>using</c> statement to ensure that a block of code
        /// is always executed with an open connection.
        /// </remarks>
        /// <example>
        /// <code>
        /// using (connection.EnsureOpen()) {
        ///   // Execute commands.
        /// }
        /// </code>
        /// </example>
        public static IDisposable EnsureOpen(this IDbConnection connection)
        {
            if (connection == null) throw new ArgumentNullException("connection");

            switch (connection.State)
            {
                case ConnectionState.Open:
                    return null;
                case ConnectionState.Closed:
                    try
                    {
                        return new ConnectionHolder(connection);
                    }
                    catch
                    {
                        try { connection.Close(); }
                        catch { } // we're already trying to handle, kthxbye
                        throw;
                    }

                default:
                    throw new InvalidOperationException("Cannot use EnsureOpen when connection is " + connection.State);
            }
        }

        private class ConnectionHolder : IDisposable
        {
            private readonly IDbConnection cn;
            private readonly bool openedLocally;

            public ConnectionHolder(IDbConnection connection)
            {                
                cn = connection;
                openedLocally = (cn.State == ConnectionState.Closed);

                if (openedLocally)
                {
                    cn.Open();
                }
            }

            public void Dispose()
            {
                if (cn != null
                    && openedLocally
                    && cn.State != ConnectionState.Closed)
                {
                    try { cn.Close(); }
                    catch {} // throwing from Dispose() is so lame
                }                
            }
        }
    }
}
